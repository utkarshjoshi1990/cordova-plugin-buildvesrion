var exec = require("cordova/exec");

var PLUGIN_NAME = "BuildVersion";

var BuildVersion = {
  setAppDetails: function(keyChainGroup, success, error) {
    
    exec(success, error, "BuildVersion", "setAppDetails", [keyChainGroup]);
  },
  getAppDetails: function(keyChainGroup, key, success, error) {
    exec(success, error, "BuildVersion", "getAppDetails", [keyChainGroup, key]);
  }
};
module.exports = BuildVersion;

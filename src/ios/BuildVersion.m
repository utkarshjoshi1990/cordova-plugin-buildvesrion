/********* BuildVersion.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>

@interface BuildVersion : CDVPlugin {
  // Member variables go here.
}

- (void)setAppDetails:(CDVInvokedUrlCommand*)command;
- (void)getAppDetails:(CDVInvokedUrlCommand*)command;
@end

@implementation BuildVersion

- (void)getAppDetails:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* groupName = [command.arguments objectAtIndex:0];
    NSString* appBundleIdAsKey = [command.arguments objectAtIndex:1];
    NSUserDefaults *getDefaults = [[NSUserDefaults alloc]
                                  initWithSuiteName:groupName];
     if (appBundleIdAsKey != nil && [appBundleIdAsKey length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"gotKey"];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }                              
    NSString* version = [getDefaults stringForKey:appBundleIdAsKey];
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:version];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)setAppDetails:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* groupName = [command.arguments objectAtIndex:0];
    NSString* appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString* appBundleIdAsKey =  [[NSBundle mainBundle] bundleIdentifier];
    if (groupName != nil && [groupName length] > 0) {
        NSUserDefaults *myDefaults = [[NSUserDefaults alloc]
                                      initWithSuiteName:groupName];
        [myDefaults setObject:appVersion forKey:appBundleIdAsKey];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:appVersion];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
   [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end
